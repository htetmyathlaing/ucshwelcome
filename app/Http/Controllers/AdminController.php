<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Selection;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index()
    {
        return view('admin.result');
    }

    public function result()
    {
        return Selection::all();
    }
}
