<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Selection;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start_time =       Carbon::create(2017, 12, 28, 11, 30, 0);
        $live_stop_time =   Carbon::create(2017, 12, 28, 12, 30, 0);
        $stop_time =        Carbon::create(2017, 12, 28, 13,  0, 0);

        $male_selections = Selection::all()->where('gender','male');
        $female_selections = Selection::all()->where('gender','female');

        return view('home',compact('male_selections', 'female_selections', 'start_time','live_stop_time', 'stop_time'));
    }
}
