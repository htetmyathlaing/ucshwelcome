<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request){
    	$id = $request->id;
    	return view('enter-activation-code', compact('id'));
    }

    public function activate(Request $request)
    {
    	$code = DB::table('activation_codes')->where('user_id', $request->id)->value('activation_code');
    	if($request->code == $code){
    		$user = User::find($request->id);
    		$user->activated = true;
    		$user->save();
    		return redirect()->route('home')->with('activation-success',"အကောင့်အတည်ပြုခြင်း အောင်မြင်ပါသည်။");
    	}
    	return redirect()->back()->withErrors(['invalidcode', 'Invalid Code']);  
    }
}
