<?php
namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
 use Twilio\Rest\Client;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'rollno' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'rollno' => $data['rollno-prefix'].$data['rollno'],
            'password' => bcrypt($data['password']),
            'is_admin' => false,
            'activated' => false
        ]);

        $twilio_sid = env('TWILIO_ACCOUNT_SID', '');
        $twilio_token = env('TWILIO_AUTH_TOKEN', '');
        $twilio_number = env('TWILIO_NUMBER', '');
        $client = new Client($twilio_sid, $twilio_token);

        $digits = 4;
        $code;
        $numbers;

        switch ($data['rollno-prefix'][0]) {
            case '1':
                $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
                $numbers = ['+959960364132', '+959969739795', '+959975305010'];
                break;
            
            case '2':
                $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
                $numbers = ['+959960364132', '+959450697225', '+959776067954'];
                break;

            case '3':
                $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
                $numbers = ['+959960364132', '+959794864397', '+959780837947'];
                break;

            case '4':
                $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
                $numbers = ['+959960364132', '+959969739795'];
                break;

            case '5':
                $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
                $numbers = ['+959960364132', '+959795989661', '+959791904869', '+959975305010'];
                break;

            case 'T':
                $code = 9999; 
                $numbers = ['+959960364132'];
                break;

            default:
                // code...
                break;
        }

        forEach($numbers as $number){
            $client->messages->create(
            // the number you'd like to send the message to
            $number,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $twilio_number,
                // the body of the text message you'd like to send
                'body' => "\n\nActivation code\n$user->rollno => $user->name => $code"
                )
            );  
        }

        DB::table('activation_codes')->insert(
            ['user_id' => $user->id, 'activation_code' => $code]
        );

        return $user;
    }
}
