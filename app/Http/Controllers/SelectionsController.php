<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Selection;
use Carbon\Carbon;

class SelectionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {
        $selection = Selection::findOrFail($request->id);
        return view('selection', compact('selection'));
    }

    public function getVotes()
    {
        $now = Carbon::now();
        $live_stop_time = Carbon::create(2017, 12, 28, 12, 30, 0); 

        if($now > $live_stop_time)
            return "stop";
        return Selection::all();
    }

    public function setVotes(Request $request)
    {   
        $user = Auth::user();
        if(!$user->activated)
            return "မဲပေးရန် အကောင့်ကို အတည်ပြုပါ။";

        $now = Carbon::now();
        $start_time =   Carbon::create(2017, 12, 28, 11, 30, 0);
        $stop_time =    Carbon::create(2017, 12, 28, 13,  0, 0); 
    
        if($now < $start_time)
            return "2017-12-28 ရက်နေ့ နံနက် ၁၁ နာရီ မတိုင်မီအထိ မဲပေးလို့ မရသေးပါ။";

        if($now > $stop_time)
            return "မဲပေးချိန်ကျော်လွန်သွားပါြပီ။";

        $selection = Selection::find($request->input('id'));
        $votedSelection;
        $return = "<strong>$selection->name</strong>&nbsp;ကို မဲေပးခဲ့သည္။!";

        switch ($request->input('vote')) {
            case 'kvote':
                if(is_null($user->kvoted)){
                    $user->kvoted = $selection->id;                    
                    $selection->kvotes = $selection->kvotes+1;
                }
                elseif (!is_null($user->kvoted) && $user->kvoted != $request->input('id')) {
                    $votedSelection = Selection::find($user->kvoted);
                    $votedSelection->kvotes = $votedSelection->kvotes-1;
                    $votedSelection->save();

                    $user->kvoted = $selection->id;                    
                    $selection->kvotes = $selection->kvotes+1;
                    $return = "<strong><span class='text-danger'>$votedSelection->name</span></strong>&nbsp;အစား &nbsp; <strong>$selection->name</strong> &nbsp; ကို ပြောင်းလဲ မဲပေးခဲ့သည်။";
                }
                elseif ($user->kvoted == $request->input('id')) {
                    $return = "<strong>$selection->name</strong>&nbsp; ကို မဲပေးပြီးပါပြီ။";
                }
                break;

            case 'qvote':
                if(is_null($user->qvoted)){
                    $user->qvoted = $selection->id;                    
                    $selection->qvotes = $selection->qvotes+1;
                }
                elseif (!is_null($user->qvoted) && $user->qvoted != $request->input('id')) {
                    $votedSelection = Selection::find($user->qvoted);
                    $votedSelection->qvotes = $votedSelection->qvotes-1;
                    $votedSelection->save();

                    $user->qvoted = $selection->id;                    
                    $selection->qvotes = $selection->qvotes+1;
                    $return = "<strong><span class='text-danger'>$votedSelection->name</span></strong>&nbsp;အစား &nbsp; <strong>$selection->name</strong> &nbsp; ကို ပြောင်းလဲ မဲပေးခဲ့သည်။";
                }
                elseif ($user->qvoted == $request->input('id')) {
                    $return = "<strong>$selection->name</strong>&nbsp; ကို မဲပေးပြီးပါပြီ။";
                }
                break;

            case 'pvote':
                if(is_null($user->pvoted)){
                    $user->pvoted = $selection->id;                    
                    $selection->pvotes = $selection->pvotes+1;
                }
                elseif (!is_null($user->pvoted) && $user->pvoted != $request->input('id')) {
                    $votedSelection = Selection::find($user->pvoted);
                    $votedSelection->pvotes = $votedSelection->pvotes-1;
                    $votedSelection->save();

                    $user->pvoted = $selection->id;                    
                    $selection->pvotes = $selection->pvotes+1;
                    $return = "<strong><span class='text-danger'>$votedSelection->name</span></strong>&nbsp;အစား &nbsp; <strong>$selection->name</strong> &nbsp; ကို ပြောင်းလဲ မဲပေးခဲ့သည်။";
                }
                elseif ($user->pvoted == $request->input('id')) {
                    $return = "<strong>$selection->name</strong>&nbsp; ကို မဲပေးပြီးပါပြီ။";
                }
                break;

            case 'ivote':
                if(is_null($user->ivoted)){
                    $user->ivoted = $selection->id;                    
                    $selection->ivotes = $selection->ivotes+1;
                }
                elseif (!is_null($user->ivoted) && $user->ivoted != $request->input('id')) {
                    $votedSelection = Selection::find($user->ivoted);
                    $votedSelection->ivotes = $votedSelection->ivotes-1;
                    $votedSelection->save();

                    $user->ivoted = $selection->id;                    
                    $selection->ivotes = $selection->ivotes+1;
                    $return = "<strong><span class='text-danger'>$votedSelection->name</span></strong>&nbsp;အစား &nbsp; <strong>$selection->name</strong> &nbsp; ကို ပြောင်းလဲ မဲပေးခဲ့သည်။";
                }
                elseif ($user->ivoted == $request->input('id')) {
                    $return = "<strong>$selection->name</strong>&nbsp; ကို မဲပေးပြီးပါပြီ။";
                }
                break;
            
            default:
                // code...
                break;
        }
        $selection->save();
        $user->save();
        return $return;
    }
}
