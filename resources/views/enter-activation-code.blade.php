@extends('layouts/app')

@section('content')
	<div class="container">
	    <div class="row justify-content-md-center mt-5">
	        <div class="col-md-8">
	            <div class="card">
	                <div class="card-header">Enter Code</div>
	                <div class="card-body">
	                    <form class="form-horizontal" method="POST" action="{{ route('activate') }}">
	                        {{ csrf_field() }}

	                        <div class="form-group row">
	                            <label for="email" class="col-lg-4 col-form-label text-lg-right">Code</label>

	                            <div class="col-lg-6">
	                                <input
	                                        type="text"
	                                        class="form-control{{ $errors->any() ? ' is-invalid' : '' }}"
	                                        name="code"
	                                        required
	                                        autofocus
	                                >
	                                <input type="hidden" name="id" value="{{ $id }}">

	                                @if ($errors->any())
	                                    <div class="invalid-feedback">
	                                        <strong>Invalid Code</strong>
	                                    </div>
	                                @endif
	                            </div>
	                        </div>

	             

	                        <div class="form-group row">
	                            <div class="col-lg-8 offset-lg-4">
	                                <button type="submit" class="btn btn-primary">
	                                    Activate
	                                </button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection