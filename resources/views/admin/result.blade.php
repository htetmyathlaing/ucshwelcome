@extends('layouts.app')

@section('css')
	<style type="text/css" media="screen">
		.kn, .qn, .pn, .in{
			width: 40%;
			font-weight: bold;
		}
		.kv, .qv, .pv, .iv{
			width: 5%;
			font-weight: bold;
		}
		.kpercent, .qpercent, .ppercent, .ipercent{
			width: 55%;
		}
		nav{
			display: none !important;
		}
	</style>
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 m-1">
				<div class="card-body bg-primary text-center text-white font-weight-bold rounded">
					<h3>Voting Live Results</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 mb-1">
				<table class="table table-striped m-1 border border-primary" id="king-table">
					<thead class="bg-primary text-white font-weight-bold">
						<tr class="text-center">
							<th colspan="3">King Results</th>
						</tr>
					</thead>
					<tbody>
						<tr id="k0">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k1">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k2">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k3">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k4">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k5">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k6">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k7">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
						<tr id="k8">
							<td class="kn"></td>
							<td class="kv"></td>
							<td class="kpercent"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6 mb-1">
				<table class="table table-striped m-1 border border-primary" id="queen-table">
					<thead class="bg-primary text-white font-weight-bold">
						<tr class="text-center">
							<th colspan="3">Queen Results</th>
						</tr>
					</thead>
					<tbody>
						<tr id="q0">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q1">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q2">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q3">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q4">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q5">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q6">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q7">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
						<tr id="q8">
							<td class="qn"></td>
							<td class="qv"></td>
							<td class="qpercent"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6 mb-1">
				<table class="table table-striped m-1 border border-primary" id="popular-table">
					<thead class="bg-primary text-white font-weight-bold">
						<tr class="text-center">
							<th colspan="3">Popular Results</th>
						</tr>
					</thead>
					<tbody>
						<tr id="p0">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p1">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p2">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p3">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p4">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p5">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p6">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p7">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
						<tr id="p8">
							<td class="pn"></td>
							<td class="pv"></td>
							<td class="ppercent"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-6 mb-1">
				<table class="table table-striped m-1 border border-primary" id="innocent-table">
					<thead class="bg-primary text-white font-weight-bold">
						<tr class="text-center">
							<th colspan="3">Innocent Results</th>
						</tr>
					</thead>
					<tbody>
						<tr id="i0">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i1">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i2">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i3">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i4">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i5">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i6">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i7">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
						<tr id="i8">
							<td class="in"></td>
							<td class="iv"></td>
							<td class="ipercent"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" ></script>
	<script type="text/javascript">
		var userCount = {{ App\User::count() }}
		// var userCount = 400;
		var maleSelection;
		var femaleSelection;
		function calculatePercentage(votes){
			var percent = (votes/userCount)*100;
			return percent.toFixed(2);
		}

		function sortKvotes(obj){
			console.log(obj);
			obj.sort(function(a, b){
			    return b.kvotes-a.kvotes
			});
		}
		function sortQvotes(obj){
			console.log(obj);
			obj.sort(function(a, b){
			    return b.qvotes-a.qvotes
			});
		}
		function sortPvotes(obj){
			console.log(obj);
			obj.sort(function(a, b){
			    return b.pvotes-a.pvotes
			});
		}
		function sortIvotes(obj){
			console.log(obj);
			obj.sort(function(a, b){
			    return b.ivotes-a.ivotes
			});
		}
		setInterval(function(){
            $.get('{{ route('votes-result') }}', function(data) {
            	maleSelection = [];
				femaleSelection = [];
				data.forEach(function(item){
					if(item.gender == "male")
						maleSelection.push(item);
					else
						femaleSelection.push(item);
				});

            	sortKvotes(maleSelection);
                for(index = 0; index < 9; index++) {
                    for(i = 0; i < 4; i++){
                    	$('#king-table > tbody >  #k'+index+' > .kn').html(maleSelection[index].name);
                    	$('#king-table > tbody >  #k'+index+' > .kv').html(maleSelection[index].kvotes);
                    	$('#king-table > tbody >  #k'+index+' > .kpercent').html(
                    		'<div class="progress" style="height:20px">'+
                    		'<div class="progress-bar bg-success" style="width:'+
                    		calculatePercentage(maleSelection[index].kvotes)+'%;height:20px">'+
                    			calculatePercentage(maleSelection[index].kvotes)+'%'+
                    		'</div>'+
                    		'</div>');
                    }
                }

                sortQvotes(femaleSelection);
                for(index = 0; index < 9; index++) {
                    for(i = 0; i < 4; i++){
                    	$('#queen-table > tbody >  #q'+index+' > .qn').html(femaleSelection[index].name);
                    	$('#queen-table > tbody >  #q'+index+' > .qv').html(femaleSelection[index].qvotes);
                    	$('#queen-table > tbody >  #q'+index+' > .qpercent').html(
                    		'<div class="progress" style="height:20px">'+
                    		'<div class="progress-bar bg-success" style="width:'+
                    		calculatePercentage(femaleSelection[index].qvotes)+'%;height:20px">'+
                    			calculatePercentage(femaleSelection[index].qvotes)+'%'+
                    		'</div>'+
                    		'</div>');
                    }
                }

                sortPvotes(maleSelection);
                for(index = 0; index < 9; index++) {
                    for(i = 0; i < 4; i++){
                    	$('#popular-table > tbody >  #p'+index+' > .pn').html(maleSelection[index].name);
                    	$('#popular-table > tbody >  #p'+index+' > .pv').html(maleSelection[index].pvotes);
                    	$('#popular-table > tbody >  #p'+index+' > .ppercent').html(
                    		'<div class="progress" style="height:20px">'+
                    		'<div class="progress-bar bg-success" style="width:'+
                    		calculatePercentage(maleSelection[index].pvotes)+'%;height:20px">'+
                    			calculatePercentage(maleSelection[index].pvotes)+'%'+
                    		'</div>'+
                    		'</div>');
                    }
                }

                sortIvotes(femaleSelection);
                for(index = 0; index < 9; index++) {
                    for(i = 0; i < 4; i++){
                    	$('#innocent-table > tbody >  #i'+index+' > .in').html(femaleSelection[index].name);
                    	$('#innocent-table > tbody >  #i'+index+' > .iv').html(femaleSelection[index].ivotes);
                    	$('#innocent-table > tbody >  #i'+index+' > .ipercent').html(
                    		'<div class="progress" style="height:20px">'+
                    		'<div class="progress-bar bg-success" style="width:'+
                    		calculatePercentage(femaleSelection[index].ivotes)+'%;height:20px">'+
                    			calculatePercentage(femaleSelection[index].ivotes)+'%'+
                    		'</div>'+
                    		'</div>');
                    }
                }
           });
        }, 1000);
	</script>
@endsection
