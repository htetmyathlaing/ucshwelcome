<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="icon" href="{{ asset('favicon.ico') }}" />
    <meta property="og:title"       content="Access Denied">
    <meta property="og:type"        content="website">
    <meta property="og:description" content="Some Description Here">
    <meta property="og:image"       content="{{ asset('img/ucsh.jpg') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <!--Material Styles Bootstrap -->

    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.0.0-beta.4/dist/css/bootstrap-material-design.min.css" integrity="sha384-R80DC0KVBO4GSTw+wZ5x2zn2pu4POSErBkf8/fSFhPXHxvHJydT0CSgAP2Yo2r4I" crossorigin="anonymous">
    
    <style>
		h1{
			font-size: 1000%;
		}
	</style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
        <div class="container">
            <a class="navbar-brand" href="#">
                {{ config('app.name', 'UCSH Fresher Welcome 2018') }}
            </a>
        </div>
    </nav>

    <div class="container mt-5">
		<div class="jumbotron text-center">
			<h1 class="text-danger">Access Denied!</h1>
	  		<h3 class="text-muted">You don't have permission to see this page.</h3> 
		</div>
	</div>

	<div class="container mt-1 text-center">
		<a class="btn btn-lg btn-primary" href="{{ route('home')}}" title="Back to Home">Back to Home</a>
	</div>
</div>
</body>
</html>