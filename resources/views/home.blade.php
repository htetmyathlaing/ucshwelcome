    @extends('layouts.app')

    @section('css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <style type="text/css">
            .carousel-inner img {
              width: 100%; /* Set width to 100% */
              height: 500px;
              margin: auto;
            }

            @-webkit-keyframes swing
            {
                0%{
                    -webkit-transform: scale(1.8);
                }
                /*15%
                {
                    -webkit-transform: translateX(5px);
                    transform: translateX(5px);
                }*/
                30%
                {
                    -webkit-transform: translateX(-5px);
                   transform: translateX(-5px);
                } 
                50%
                {
                    -webkit-transform: translateX(3px);
                    transform: translateX(3px);
                }
                65%
                {
                    -webkit-transform: translateX(-3px);
                    transform: translateX(-3px);
                }
                80%
                {
                    -webkit-transform: translateX(2px);
                    transform: translateX(2px);
                }
                100%
                {
                    -webkit-transform: translateX(0);
                    transform: translateX(0);
                }
            }
            @keyframes swing
            {
                0%{
                    -ms-transform: scale(1.8);
                    transform: scale(1.8);
                }
                /*15%
                {
                    -webkit-transform: translateX(5px);
                    transform: translateX(5px);
                }*/
                30%
                {
                    -webkit-transform: translateX(-5px);
                    transform: translateX(-5px);
                }
                50%
                {
                    -webkit-transform: translateX(3px);
                    transform: translateX(3px);
                }
                65%
                {
                    -webkit-transform: translateX(-3px);
                    transform: translateX(-3px);
                }
                80%
                {
                    -webkit-transform: translateX(2px);
                    transform: translateX(2px);
                }
                100%
                {
                    -webkit-transform: translateX(0);
                    transform: translateX(0);
                }
            }

            .nav-link{
                font-size: 80% !important;
            }

            .thumbnail > .img :hover{
                cursor: pointer;
                /*border: 1px solid #ddd; */
                /*-webkit-transform: scale(1.3);
                -ms-transform: scale(1.3);
                transform: scale(1.3);*/
                border-radius:5%;
                -webkit-animation: swing 1s ease;
                animation: swing 1s ease;
                -webkit-animation-iteration-count: 1;
                animation-iteration-count: 1;
            }

           .caption :hover{
                cursor: default;
                border: 0 !important; 
                animation: none !important;
            }

            .thumbnail > .img > img{
                width:100%;
                height: 300px;
            }
            .modal-backdrop {
                opacity: 0.7 !important;
            }
            .carousel-caption{
                font-weight: bold;
                text-shadow: 2px 1px #000, 2px 2px 8px #4B96CC;
            }
            .sticky-top{
                width: 100%;
                background-color: #fff;
            }

            .img{
                position: relative;
            }

            .badge-h4{
                position: absolute;
                top: 2px;
                right: 2px;
            }
            .thumbnail{
                -webkit-box-shadow: 0 0 10px #c3c3c3;
                   -moz-box-shadow: 0 0 10px #c3c3c3;
                        box-shadow: 0 0 10px #c3c3c3;
            }

            .btn-wrap{
                display: inline-block;
            }

            .btn-wrap > .btn:hover{
                border: 1px solid transparent !important;
            }
            
            .countback{
                margin-bottom: 20px;
            }
            .countback span{
                font-size: 150%;
                font-weight: bold;
                color: #4caf50;
            }
            .countback .live-start{
                -webkit-transform: scale(1.8);
                    -ms-transform: scale(1.8);
                        transform: scale(1.8);
            }

            .countback .timeout{
                color: #cb1212 !important;
            } 

            .countback #count-back-text{
                font-size: 100% !important;
                display: block;
                margin-top: 20px;
                color: #2296f4;
                font-weight: normal;
            }

            /* Portrait */
            @media screen and (orientation:portrait) and (max-width: 576px){
                /* Portrait styles */
                .carousel-inner img {
                  width: 100%; /* Set width to 100% */
                  height: auto;
                  margin: auto;
                }

                .btn-wrap{
                    display: block;
                }

                .thumbnail > .img > img{
                    width:100%;
                    height: 200px;
                }
            }        
        </style>
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    @endsection

    @section('content')
        {{-- Carousel --}}
        <div id="ucsh-carousel" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#ucsh-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#ucsh-carousel" data-slide-to="1"></li>
                <li data-target="#ucsh-carousel" data-slide-to="2"></li>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                    {{-- <div class="test-img"></div> --}}
                    <div class="carousel-caption">
                        <h3>UCSH Fresher Welcome 2018</h3>
                        <p>Let's vote King & Queen of this year!</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                    {{-- <div class="test-img"></div> --}}
                    <div class="carousel-caption">
                        <h3>UCSH Fresher Welcome 2018</h3>
                        <p>Let's vote King & Queen of this year!</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                    {{-- <div class="test-img"></div> --}}
                    <div class="carousel-caption">
                        <h3>UCSH Fresher Welcome 2018</h3>
                        <p>Let's vote King & Queen of this year!</p>
                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#ucsh-carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#ucsh-carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>

        <div class="contatiner text-center mt-1">
            <h5 class="countback">
                <span id="count-back-text"></span><br>
                <span id="count-back-days"></span>
                <span id="count-back-hours"></span>
                <span id="count-back-minutes"></span>
                <span id="count-back-seconds"></span>
            </h5>
        </div>

        {{-- Tabs --}}
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs sticky-top">
                <li class="nav-item text-center" style="width: 25%">
                    <a class="nav-link  p-3 active" data-toggle="tab" href="#king">King</a>
                </li>
                <li class="nav-item text-center" style="width: 25%">
                    <a class="nav-link p-3" data-toggle="tab" href="#queen">Queen</a>
                </li>
                <li class="nav-item text-center" style="width: 25%">
                    <a class="nav-link p-3" data-toggle="tab" href="#popular">Popular</a>
                </li>
                <li class="nav-item text-center" style="width: 25%">
                    <a class="nav-link p-3" data-toggle="tab" href="#innocent">Innocent</a>
                </li>
            </ul>

            @if(!Auth::user()->activated)
                <div class="container-fluid text-center m-2">
                    <a class="text-info" href={{ route('activate-user', ['id' => Auth::user()->id]) }}>မဲပေးရန်အတွက် အကောင့်ကို အတည်ပြုရန် နှိပ်ပါ။</a>
                </div>
            @endif

            <!-- Tab panes -->
            <div class="tab-content">
                {{-- King --}}
                <div class="tab-pane active container-fluid" id="king">
                    <div class="card mt-1 mb-1">
                        <div class="card-header text-center">
                            King Selection
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($male_selections as $selection)
                                    <div class="col-6 col-md-4 col-lg-2 mb-2">
                                        <div class="thumbnail text-center mb-2 pb-2">
                                            <div class="img">
                                                <img src="{{ asset('img/'.$selection->rollno.'.jpg') }}" alt="King Selection">
                                                <h4 class="badge-h4"><span class="badge  badge-success" id="kvotes{!! $selection->id !!}">0 Votes</span></h4>
                                            </div>
                                            <div class="caption mt-2">
                                                <p>{!! $selection->name !!}</p>
                                                <a class="btn btn-success" href="{{ URL::to('/selection/'.$selection->id)}}">View</a>
                                                <div class="btn-wrap">
                                                    @if(Auth::user()->kvoted == $selection->id)
                                                        <button class="btn bg-success text-white kvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='kvote'>Voted</button>
                                                    @else
                                                        <button class="btn btn-outline-primary kvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='kvote' @php if(!Auth::user()->activated) echo 'disabled' @endphp>Vote</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Queen Tab --}}
                <div class="tab-pane container-fluid fade" id="queen">
                    <div class="card mt-1 mb-1">
                        <div class="card-header text-center">
                            Queen Selection
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($female_selections as $selection)
                                    <div class="col-6 col-md-4 col-lg-2 mb-2">
                                        <div class="thumbnail text-center">
                                            <div class="img">
                                                <img src="{{ asset('img/'.$selection->rollno.'.jpg') }}" alt="Queen Selection">
                                                <h4 class="badge-h4"><span class="badge  badge-success" id="qvotes{!! $selection->id !!}">0 Votes</span></h4>
                                            </div>
                                            <div class="caption mt-2">
                                                <p>{!! $selection->name !!}</p>
                                                <a class="btn btn-success" href="{{ URL::to('/selection/'.$selection->id)}}">View</a>
                                                <div class="btn-wrap">
                                                    @if(Auth::user()->qvoted == $selection->id)
                                                        <button class="btn bg-success text-white qvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='qvote'>Voted</button>
                                                    @else
                                                        <button class="btn btn-outline-primary qvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='qvote' @php if(!Auth::user()->activated) echo 'disabled' @endphp>Vote</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Popular --}}
                <div class="tab-pane container-fluid fade" id="popular">
                    <div class="card mt-1 mb-1">
                        <div class="card-header text-center">
                            Popular Selection
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($male_selections as $selection)
                                    <div class="col-6 col-md-4 col-lg-2 mb-2">
                                        <div class="thumbnail text-center">
                                            <div class="img">
                                                <img src="{{ asset('img/'.$selection->rollno.'.jpg') }}" alt="Popular Selection">
                                                <h4 class="badge-h4"><span class="badge  badge-success" id="pvotes{!! $selection->id !!}">0 Votes</span></h4>
                                            </div>
                                            <div class="caption mt-2">
                                                <p>{!! $selection->name !!}</p>
                                                <a class="btn btn-success" href="{{ URL::to('/selection/'.$selection->id)}}">View</a>
                                                <div class="btn-wrap">
                                                    @if(Auth::user()->pvoted == $selection->id)
                                                        <button class="btn bg-success text-white pvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='pvote'>Voted</button>
                                                    @else
                                                        <button class="btn btn-outline-primary pvote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='pvote' @php if(!Auth::user()->activated) echo 'disabled' @endphp>Vote</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                

                {{-- Innocent Tab --}}
                <div class="tab-pane container-fluid fade" id="innocent">
                    <div class="card mt-1 mb-1">
                        <div class="card-header text-center">
                            Innocent Selection
                        </div>
                        <div class="card-body">
                            <div class="row">
                                @foreach($female_selections as $selection)
                                    <div class="col-6 col-md-4 col-lg-2 mb-2">
                                        <div class="thumbnail text-center">
                                            <div class="img">
                                                <img src="{{ asset('img/'.$selection->rollno.'.jpg') }}" alt="Innocent Selection">
                                                <h4 class="badge-h4"><span class="badge  badge-success" id="ivotes{!! $selection->id !!}">0 Votes</span></h4>
                                            </div>
                                            <div class="caption mt-2">
                                                <p>{!! $selection->name !!}</p>
                                                <a class="btn btn-success" href="{{ URL::to('/selection/'.$selection->id)}}">View</a>
                                                <div class="btn-wrap">
                                                    @if(Auth::user()->ivoted == $selection->id)
                                                        <button class="btn bg-success text-white ivote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='ivote'>Voted</button>
                                                    @else
                                                        <button class="btn btn-outline-primary ivote" id="{!! $selection->id !!}" name="{!! $selection->name !!}" data-vote='ivote' @php if(!Auth::user()->activated) echo 'disabled' @endphp>Vote</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="card">
                <div class="card-header text-center">
                    <p>University of Computer Studies, Hinthada<br>Fresher Welcome 2018</p>
                </div>
            </div>
        </footer>
        
    @endsection

    @section('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
       
        <script type="text/javascript">

            toastr.options = {
                "closeButton": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "timeOut": "2000"
            }

            var msg = "{{ Session::has('activation-success') ? session('activation-success') : "" }}";
            if(msg.length > 0)
                toastr.success(msg);

            
            var start_time = new Date('{!! $start_time !!}');
            var live_stop_time = new Date('{{ $live_stop_time }}');
            var stop_time = new Date('{!! $stop_time !!}');
            var now = new Date().getTime();
            var distance = live_stop_time - now;
            
            function countbackToStop(){

                var countback_to_stop = setInterval(function() {
                    $('.countback span').removeClass('live-start');

                    now = new Date().getTime();

                    distance = stop_time - now;

                    if(distance < 60000){
                        $('.countback span').addClass('timeout');
                        $('.countback #count-back-text').removeClass('timeout');
                    }
                    
                    if (distance < 0) {
                        clearInterval(countback_to_stop);
                        $('.countback span').addClass('timeout');
                        $("#count-back-text").html("Voting live result stopped");
                        $("#count-back-hours").html("");
                        $("#count-back-minutes").html("");
                        $("#count-back-seconds").html(""); 
                        return; 
                    }

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    $("#count-back-text").html("Voting live result will stop in ");
                    $("#count-back-days").html("");
                    $("#count-back-hours").html(hours + " : ");
                    $("#count-back-minutes").html(minutes + " : ");
                    $("#count-back-seconds").html(seconds);
                }, 1000);
            }

            if(stop_time - now < 0){
                $('.countback span').addClass('timeout')
                $("#count-back-text").html("Voting Timeout");
            }
            else if (distance < 0) {
                $('.countback span').addClass('timeout')

                $("#count-back-text").html("Voting live result stopped");
                $("#count-back-hours").html("");
                $("#count-back-minutes").html("");
                $("#count-back-seconds").html("");
            }
            else{
                var countback_to_start = setInterval(function() {
                    var now = new Date().getTime();

                    var distance = start_time - now;

                    if (distance < 0) {
                        clearInterval(countback_to_start);
                        countbackToStop();
                        $('.countback span').addClass('live-start');
                        $("#count-back-text").html("");
                        $("#count-back-hours").html("Voting start!");
                        $("#count-back-minutes").html("");
                        $("#count-back-seconds").html("");
                        return;
                    }

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    $("#count-back-text").html("Voting will start in ");
                    if(days > 1)
                        $("#count-back-days").html(days + " Days");
                    else if(days == 1)
                        $("#count-back-days").html(" Tomorrow");
                    else{
                        $("#count-back-hours").html(hours + " : ");
                        $("#count-back-minutes").html(minutes + " : ");
                        $("#count-back-seconds").html(seconds);
                    }
                }, 1000);  
            }

           $('.kvote, .qvote, .pvote, .ivote').click(function(event) {
                var btn= $(this);
                $.ajax({ 
                    url   : '{{ route('setvotes') }}',
                    type  : 'POST',
                    data  : {
                        'id': $(this).attr('id'),
                        'vote':$(this).data('vote')
                        },
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    success: function(result){ 
                        switch(btn.data('vote')){
                            case 'kvote':{
                                $('.kvote').html("Vote");
                                $('.kvote').removeClass('bg-success text-white');
                                $('.kvote').addClass('btn-outline-primary');
                                btn.html("Voted");
                                btn.addClass('bg-success text-white');
                                toastr.success(result);
                                break;
                            }
                            case 'qvote':{
                                $('.qvote').html("Vote");
                                $('.qvote').removeClass('bg-success text-white');
                                $('.qvote').addClass('btn-outline-primary');
                                btn.html("Voted");
                                btn.addClass('bg-success text-white');
                                toastr.success(result);
                                break;
                            }
                            case 'pvote':{
                                $('.pvote').html("Vote");
                                $('.pvote').removeClass('bg-success text-white');
                                $('.pvote').addClass('btn-outline-primary');
                                btn.html("Voted");
                                btn.addClass('bg-success text-white');
                                toastr.success(result);
                                break;
                            }
                            case 'ivote':{
                                $('.ivote').html("Vote");
                                $('.ivote').removeClass('bg-success text-white');
                                $('.ivote').addClass('btn-outline-primary');
                                btn.html("Voted");
                                btn.addClass('bg-success text-white');
                                toastr.success(result);
                                break;
                            }
                        }   
                    }
                });
           });

            var liveresult = setInterval(function(){
                $.get('{{ route('votes') }}', function(data) {
                    if(data == "stop"){
                        console.log("live stop");
                        clearInterval(liveresult);
                        return;
                    }
                    
                    data.forEach(function (item) {
                        $('#kvotes'+item.id).html(item.kvotes+" Votes");
                        $('#qvotes'+item.id).html(item.qvotes+" Votes");
                        $('#pvotes'+item.id).html(item.pvotes+" Votes");
                        $('#ivotes'+item.id).html(item.ivotes+" Votes");
                    });
               });
            }, 1000);

          
        </script>
    @endsection
