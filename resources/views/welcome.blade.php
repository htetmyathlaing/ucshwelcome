@extends('layouts.app')

@section('css')
    <style type="text/css">
        .carousel-inner img {
          width: 100%; /* Set width to 100% */
          height: auto;
          margin: auto;
        }
/*
        .test-img{
            width: 100%;
            height: 500px;
            background-color: gray;
        }
*/
        .carousel-caption{
            font-weight: bold;
            text-shadow: 2px 1px #000, 2px 2px 8px #4B96CC;
        }

        .carousel-inner img {
                  width: 100%; /* Set width to 100% */
                  height: 500px;
                  margin: auto;
                }

        /* Portrait */
            @media screen and (orientation:portrait) and (max-width: 576px){
                /* Portrait styles */
                .carousel-inner img {
                  width: 100%; /* Set width to 100% */
                  height: auto;
                  margin: auto;
                }
            }

            @media screen and (min-width: 576px){

                .card{
                    margin-left: 50px !important;
                    margin-right: 50px !important;*/
                }
            }     
    </style>
@endsection

@section('content')
    {{-- Carousel --}}
    <div id="ucsh-carousel" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#ucsh-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#ucsh-carousel" data-slide-to="1"></li>
            <li data-target="#ucsh-carousel" data-slide-to="2"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                {{-- <div class="test-img"></div> --}}
                <div class="carousel-caption">
                    <h3>UCSH Fresher Welcome 2018</h3>
                    <p>Let's vote King & Queen of this year!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                {{-- <div class="test-img"></div> --}}
                <div class="carousel-caption">
                    <h3>UCSH Fresher Welcome 2018</h3>
                    <p>Let's vote King & Queen of this year!</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/ucsh.jpg') }}" alt="UCSH">
                {{-- <div class="test-img"></div> --}}
                <div class="carousel-caption">
                    <h3>UCSH Fresher Welcome 2018</h3>
                    <p>Let's vote King & Queen of this year!</p>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#ucsh-carousel" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#ucsh-carousel" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>

    <div class="card mt-4 m-1">
        <div class="card-header bg-primary text-white">
            Fresher Welcome ဆိုတာ ဘာလဲ?
        </div>
        <div class="card-body">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="text-info">Fresher Welcome</strong> ဆိုတာ ဒီနှစ်မှာ တက္ကသိုလ်ကို စတင်ရောက်ရှိလာတဲ့ ပထမနှစ်မှ မောင်မယ်သစ်လွင်များကို ကျောင်းမှာရှိတဲ့ အကိုအမများက ကြိုဆိုဧည့်ခံတဲ့ ပွဲလေးပါ။ ကျောင်းရဲ Activity တွေနဲ့ ပတ်သက်ပြီး ရင်နှီးလာအောင်ရယ် ကျောင်းမှာရှိတဲ့ ကျောင်းသားကျောင်းသူများအချင်းချင်း ပိုမိုရင်းနှီးခင်မင်လာအောင် ပျော်ပွဲရွှင်ပွဲအနေနဲ့ ကျင်းပကြတာ ဖြစ်ပါတယ်။<br>
            မောင်မယ်သစ်လွင်တွေအတွက်ကတော့ <strong class="text-info"> Fresher Welcome </strong> ဆိုတဲ့ <strong class="text-info"> မောင်မယ်သစ်လွင်ကြိုဆိုပွဲ</strong> ဟာ တစ်သက်တာ အမှတ်တရတွေ ဖြစ်စေမှာ အသေအချာပါပဲ။
        </div>
    </div> 

    <div class="card mt-4 m-1">
        <div class="card-header bg-primary text-white">
            Fresher Welcome မှာ ဘာတွေလုပ်ကြသလဲ?
        </div>
        <div class="card-body">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ပထမနှစ်မှ ကျောင်းသားကျောင်းသူတွေထဲကနေပြီးတော့<strong class="text-info"> King & Queen </strong> ရယ်၊ လူကြိုက်အများဆုံး ယောက်ျားလေး <strong class="text-info"> Popular Boy </strong> ရယ်၊  လူကြိုက်အများဆုံး မိန်းကလေး <strong class="text-info">  Innocent Girl </strong> ရယ် စုစုပေါင်း လေးယောက်ကို မဲပေးရွေးချယ်ပါတယ်။
            <br> ပထမနှစ်မှ မောင်မယ်သစ်လွင်များအပါအဝင် အကိုအမတွေနဲ့ အတူတူ တက်ရောက်လာသူများကို အဆိုအကတွေနဲ့ ဖျော်ဖြေတင်ဆက်ကြမှာပါ။
        </div>
    </div>

    <div class=" mt-4 m-1">
        <div class="text-center">
            Please login to participate!
        </div>
        <div class="text-center">
            <a class="btn btn-primary m-2" href="{{ route('login') }}">Login</a></li>
            <a class="btn btn-primary m-2" href="{{ route('register') }}">Register</a></li>
        </div>
    </div>

    
    <footer class="text-center m-3">
      {{-- <div class="card">
        <div class="card-header mb-1 text-center"> --}}
            <p>University of Computer Studies, Hinthada<br>Fresher Welcome 2018</p>
        {{-- </div>
      </div>   --}}
    </footer>

@endsection