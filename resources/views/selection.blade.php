@extends('layouts.app')

@section('css')
	<style type="text/css" media="screen">
		.thumbnail img{
			width: 100%;
		}

		td{
			border: none !important;
			width: 50%;
		}
	</style>
@endsection
@section('content')
	<div class="container mt-5">
        <div class="card">
            <div class="card-body">
            	<div class="row">
            		<div class="col-lg-6">
	         			<div class="thumbnail text-center mb-2 pb-2">
	         				<img src="{{ asset('img/'.$selection->rollno.'.jpg') }}" alt="Queen Selection">
	         			</div>
	         		</div>
	         		<div class="col-lg-6">
	         			<table class="table">
	         				<tbody>
	         					<tr>
	         						<td class="text-right">Name :</td>
	         						<td class="text-left">{{ $selection->name }}</td>
	         					</tr>
	         					<tr>
	         						<td class="text-right">Age :</td>
	         						<td class="text-left">{{ $selection->age }}</td>
	         					</tr>
	         					<tr>
	         						<td class="text-right">Height :</td>
	         						<td class="text-left">{{ $selection->height }}</td>
	         					</tr>
	         					<tr>
	         						<td class="text-right">From :</td>
	         						<td class="text-left">{{ $selection->from }}</td>
	         					</tr>
	         					<tr>
	         						<td class="text-right">Interested in :</td>
	         						<td class="text-left">{{ $selection->interested_in }}</td>
	         					</tr>
	         					<tr>
	         						<td class="text-right">Facebook :</td>
	         						<td class="text-left"><a href="{{ $selection->fb_link }}">{{ $selection->fb_name }}</a></td>
	         					</tr>
	         				</tbody>
	         			</table>
	         		</div>
            	</div>
            </div>
        </div>
    </div>
    <div class="container mt-2 mb-2 text-center">
		<a class="btn btn-lg btn-primary" href="{{ route('home')}}" title="Back to Home">Back to Home</a>
	</div>
@endsection