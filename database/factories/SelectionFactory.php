<?php

use Faker\Generator as Faker;

$factory->define(App\Selection::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'rollno' => '1CST-'.$faker->numberBetween($min = 1, $max = 100),
        'gender' => 'male',  
        'kvotes' => $faker->numberBetween($min = 0, $max = 400), 
        'qvotes' => $faker->numberBetween($min = 0, $max = 400),
        'pvotes' => $faker->numberBetween($min = 0, $max = 400), 
        'ivotes' => $faker->numberBetween($min = 0, $max = 400), 
    ];
});
