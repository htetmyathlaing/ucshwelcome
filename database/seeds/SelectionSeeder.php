<?php

use Illuminate\Database\Seeder;

class SelectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $selections = [
            ['name' => '​စောခရစ္စတိုဖာ' , 'rollno' => 'm1' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 7 in' , 'from' => 'ဓနုဖြူ' ,  'interested_in' => 'စာဖတ်ခြင်း' , 'fb_name' => '', 'fb_link' => '' ],

            ['name' => 'မောင်ကျော်ဇင်အောင်' , 'rollno' => 'm2' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ရွာသစ်' ,  'interested_in' => 'မော်ဒယ်နှင့် သရုပ်ဆောင်' , 'fb_name' => 'Kyaw Zin Aung', 'fb_link' => 'https://www.facebook.com/profile.php?id=100017060264290' ],
            
            ['name' => 'မောင်ဟန်ဝင်းမောင်' , 'rollno' => 'm3' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ကွင်းကောက်' , 'interested_in' => 'စာဖတ်ခြင်း', 'fb_name' => 'Thet Myat', 'fb_link' => ''],

            ['name' => 'မောင်လွင်မိုးအောင်' , 'rollno' => 'm4' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'မှော်ဘီ' ,  'interested_in' => 'အနုပညာ' , 'fb_name' => 'Lwin Moe Aung', 'fb_link' => 'https://www.facebook.com/profile.php?id=100022970198915' ],

            ['name' => 'မောင်ဟိန်းသူ' , 'rollno' => 'm5' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 4 in' , 'from' => 'မအူပင်' ,  'interested_in' => 'အကစားနှင့် အက' , 'fb_name' => 'Hein Thu', 'fb_link' => 'https://www.facebook.com/profile.php?id=100019099693380' ],

            ['name' => 'မောင်ဇော်လွင်ထူး' , 'rollno' => 'm6' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 4 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'စာဖတ်ခြင်း' , 'fb_name' => 'Zaw Lwin Htoo', 'fb_link' => '' ],

            ['name' => 'မောင်မြတ်မွန်စိုး' , 'rollno' => 'm7' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 4 in' , 'from' => 'ကြံခင်း' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Myat Mon Soe', 'fb_link' => 'https://www.facebook.com/tharsoe.lay.1690' ],
             
            ['name' => 'မောင်ထက်ဇေယျာဝင်း' , 'rollno' => 'm8' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ကြံခင်း' ,  'interested_in' => 'အနုပညာ' , 'fb_name' => 'Xtet Zaya', 'fb_link' => 'https://www.facebook.com/profile.php?id=100009729001587' ],

            ['name' => 'မောင်နိုင်ဝင်းကို' , 'rollno' => 'm9' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 6 in' , 'from' => 'ကွင်းကောက်' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Naing Win Ko', 'fb_link' => 'https://www.facebook.com/bansulleo.chargner.14' ],

            ['name' => 'မောင်ကျော်စွာဟိန်း' , 'rollno' => 'm10' , 'gender' => 'male' , 'age' => 17 , 'height' => '5 ft 6 in' , 'from' => 'ဘိုကလေး' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Kyaw Swar Hein', 'fb_link' => 'https://www.facebook.com/profile.php?id=100022997405234' ],

            ['name' => 'မောင်မြတ်သူနိုင်' , 'rollno' => 'm11' , 'gender' => 'male' , 'age' => 17 , 'height' => '6 ft' , 'from' => 'ရန်ကုန်' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Myat Thu Naing', 'fb_link' => 'https://www.facebook.com/myat.thunaing.3' ],

             
            ['name' => 'မသဲဆုလွင်' , 'rollno' => 'f1' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ဒူးယား' ,  'interested_in' => 'မော်ဒယ်' , 'fb_name' => 'Thae Kabyar Po', 'fb_link' => 'https://www.facebook.com/profile.php?id=100015692114804' ],

            ['name' => 'မမဉ္ဇူခိုင်ထွန်း' , 'rollno' => 'f2' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 4 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'ရေကူးခြင်း' , 'fb_name' => '', 'fb_link' => '' ],

            ['name' => 'ယွန်းနဒီခန့်' , 'rollno' => 'f3' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 1 in' , 'from' => 'ကွင်းကောက်' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Yoon Nadi Khank', 'fb_link' => 'https://www.facebook.com/yoonnadi.khank.5' ],
            
            ['name' => 'မမြဖူးငုံ' , 'rollno' => 'f4' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 3 in' , 'from' => 'ရန်ကုန်' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Mya Phoo Ngone', 'fb_link' => 'https://www.facebook.com/myaphoo.ngone.39' ],

            ['name' => 'မငုဝါထွန်း' , 'rollno' => 'f5' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 2 in' , 'from' => 'ဇလွန်' ,  'interested_in' => 'စာဖတ်ခြင်း' , 'fb_name' => 'NguWah', 'fb_link' => 'https://www.facebook.com/nguwah.nguwah.146' ],

            ['name' => 'မရွှေယမုံအေး' , 'rollno' => 'f6' , 'gender' => 'female' , 'age' => 17 , 'height' => '4 ft 11 in' , 'from' => 'သုံးခွ' ,  'interested_in' => 'အက' , 'fb_name' => 'Lamin Aye', 'fb_link' => 'https://www.facebook.com/lamin.aye.52206' ],

            ['name' => 'မသစ္စာနွေးထွေး' , 'rollno' => 'f7' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 1 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'သီချင်းဆိုခြင်း' , 'fb_name' => 'Thit Sar Nway Htwe', 'fb_link' => 'https://www.facebook.com/profile.php?id=100015326327141' ],

            ['name' => 'မဇင်မီမီဇော်' , 'rollno' => 'f8' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 1 in' , 'from' => 'လေးမျက်နှာ' ,  'interested_in' => 'သီချင်းဆိုခြင်း' , 'fb_name' => 'Khay Mee', 'fb_link' => 'https://www.facebook.com/khinmyanmar.naing.777' ],

            ['name' => 'မအိမ့်သဉ္ဇာကျော်' , 'rollno' => 'f9' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 3 in' , 'from' => 'ရန်ကုန်' ,  'interested_in' => 'ရေကူးခြင်း' , 'fb_name' => '', 'fb_link' => '' ],

            ['name' => 'မပြည့်ဇင်သူ' , 'rollno' => 'f10' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 4 in' , 'from' => 'ကျုံပျော်' ,  'interested_in' => 'အားကစား' , 'fb_name' => 'Zin Lay', 'fb_link' => '' ],

            ['name' => 'မလွင်မြတ်နိုးကျော်' , 'rollno' => 'f11' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 6 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'ပန်းချီ' , 'fb_name' => '', 'fb_link' => '' ],

            ['name' => 'မဝိုင်းမေဇင်' , 'rollno' => 'f12' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'သီချင်းဆိုခြင်း' , 'fb_name' => 'Wyine May Zin', 'fb_link' => 'https://www.facebook.com/wyinemay.zin' ],

            ['name' => 'မအိသိင်္ဂီကျော်' , 'rollno' => 'f13' , 'gender' => 'female' , 'age' => 17 , 'height' => '5 ft 5 in' , 'from' => 'ဟင်္သာတ' ,  'interested_in' => 'သီချင်းဆိုခြင်း' , 'fb_name' => '', 'fb_link' => '' ]
        ];

        foreach($selections as $selection){
		    App\Selection::create($selection);
		}
    }
}
