<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'name' => 'Admin',
        	'rollno' => 'admin',
        	'password' => bcrypt('adminadmin'),
        	'is_admin' => true,
            'activated' => true
        ]);
    }
}
