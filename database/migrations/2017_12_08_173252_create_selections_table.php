<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSelectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('rollno');
            $table->string('gender');
            $table->integer('age');
            $table->string('height');
            $table->string('from');
            $table->string('interested_in');
            $table->string('fb_name');
            $table->string('fb_link');
            $table->integer('kvotes')->default(0);
            $table->integer('qvotes')->default(0);
            $table->integer('pvotes')->default(0);
            $table->integer('ivotes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selections');
    }
}
