<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
	if (Auth::check())
		return redirect()->action('HomeController@index');	
	else
    	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/selection/{id}', 'SelectionsController@show')->name('selection');

Route::get('/votes', 'SelectionsController@getVotes')->name('votes');

Route::post('/votes', 'SelectionsController@setVotes')->name('setvotes');


Route::get('/denied', function(){
	return view('errors.permission-denied');
})->name('denied');


Route::get('/admin', 'AdminController@index');
Route::get('/admin/result', 'AdminController@result')->name('votes-result');;

Route::get('/activate-user', 'UserController@index')->name('activate-user');
Route::post('/activate', 'UserController@activate')->name('activate');


